import {createMuiTheme} from '@material-ui/core';

export const theme = createMuiTheme({
  palette: {
    common: {
      black: '#000000',
      white: '#FFFFFF',
    },
    type: 'light',
    primary: {
      main: '#0058F0',
      light: '#718EF6',
      dark: '#004DD3',
      contrastText: '#FFFFFF',
    },
    secondary: {
      main: '#03DAC5',
      light: '#70EFDE',
      dark: '#018786',
      contrastText: '#000000',
    },
    error: {
      main: '#B00020',
      light: '#E94948',
      dark: '#790000',
      contrastText: '#FFFFFF',
    },
  },
  typography: {
    fontFamily: ['"Roboto"', 'sans-serif'],
    // letter spacing missing from default theme
    h1: {
      letterSpacing: '-0.01562em',
    },
    h2: {
      letterSpacing: '-0.00833em',
    },
    h3: {
      letterSpacing: '0em',
    },
    h4: {
      letterSpacing: '0.00735em',
    },
    h5: {
      letterSpacing: '0em',
    },
    h6: {
      letterSpacing: '0.0075em',
    },
    subtitle1: {
      letterSpacing: '0.00938em',
    },
    subtitle2: {
      letterSpacing: '0.00714em',
    },
    body1: {
      letterSpacing: '0.00938em',
    },
    body2: {
      letterSpacing: '0.01071em',
    },
    button: {
      letterSpacing: '0.02857em',
    },
    caption: {
      letterSpacing: '0.03333em',
    },
    overline: {
      letterSpacing: '0.08333em',
    },
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: '0.75rem',
        backgroundColor: '#616161E6',
      },
    },
  },
});


