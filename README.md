# DAaaS Components

To get set up:

- Install VS Code: [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
- Add the ESlint extension to VS Code
- Install npm: [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)
- Install yarn: `npm install --global yarn`

To run:

- `yarn install`
- `yarn storybook`
- Open [http://localhost:6006/](http://localhost:6006/)

## Tutorials

- [Design Systems for Developers](https://www.learnstorybook.com/design-systems-for-developers/)
